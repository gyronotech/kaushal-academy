<?php
session_start();
include_once 'conection.php';

$username = $_SESSION['username'];

if(!isset($_SESSION['username']))
{
 header("Location: index.php");
}

if(isset($_GET['logout']))
{
  session_destroy();
  unset($_SESSION['username']);
  unset($_SESSION['user_type']);
  header("Location: login.php");
}
?>
