<?php
include_once "admin/conection.php";
$cookie_name = 'anyar';
if (isset($_GET['t']))
{
  $course_name = $_GET['t'];
}

$get_name = $conn->prepare("SELECT first_name FROM user_website WHERE username = ?");
$get_name->bind_param('s', $_COOKIE[$cookie_name]);
$get_name->execute();
$get_name_rs = $get_name->get_result();
$get_name_row = $get_name_rs->fetch_assoc();

$fetch_topic = $conn->prepare("SELECT * FROM topic_website WHERE course_name = ? ");
$fetch_topic->bind_param('s', $course_name);
$fetch_topic->execute();
$fetch_topic_res = $fetch_topic->get_result();

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Topic-Kaushal Skill Academy</title>
  

  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

<!--plugn for the video -->
  <link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />


  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Anyar - v2.2.1
  * Template URL: https://bootstrapmade.com/anyar-free-multipurpose-one-page-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-inner-pages">
    <div class="container d-flex align-items-center">
      <div class="contact-info mr-auto">
      </div>
      <div class="cta">
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.php#header" class="scrollto"><h5>KAUSHAL SKILL ACADEMY</h5></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="index.php#header">Home</a></li>
          <li><a href="index.php#about">About</a></li>
          <li><a href="index.php#services">Services</a></li>
          <li><a href="index.php#contact">Contact</a></li>
          <li><a href="index.php#team">Team</a></li>
          <?php

if(isset($_COOKIE[$cookie_name]) != '')

{

  ?>
         
          <li><a href="live.php">Live Class</a></li>
         
          <li class="active"><a href="course.php">Courses</a></li>
          <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <?php echo $get_name_row['first_name']; ?> <span class="fa fa-angle-down"></span></a>
          <!--<ul class="dropdown-menu" role="menu">-->
          <li><a href="logout.php?logout">Logout</a></li>

        </ul>
        </li> <?php
        
      }

      else

      {

        ?>

<li><a href="login.php?login">Courses</a></li>

<li><a href="login.php?login">Blog</a></li>

<li><a href="login.php?login">Live Classes</a></li>

<li><a href="login.php">Login</a></li>

<?php

}
?>
</ul>
</div>
</div>

      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="course.php">Course</a></li>
          <li><a>Topic</a></li>
        </ol>
   

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container">

        <div class="row">

        <div class="col-lg-11 entries">

<article class="entry entry-single">
    <br>
                <!-- start course content container -->
                <div class="mu-course-container">
                  <!-- <div class="clearfix"> -->
                    <?php
                    while ($fetch_topic_row = $fetch_topic_res->fetch_assoc())
                    {
                      ?>
                      <div class="col-md-6 col-sm-6">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="topic_detail.php?v=<?php echo $fetch_topic_row['topic_name']; ?>"><img src="<?php echo $fetch_topic_row['topic_img']; ?>" alt="img"></a>
                        </figure>
                        <div class="mu-latest-course-single-content">
                          <h4><a href="topic_detail.php?v=<?php echo $fetch_topic_row['topic_name']; ?>"><?php echo stripslashes($fetch_topic_row['topic_name']); ?></a></h4>
                          <p><?php echo stripslashes($fetch_topic_row['topic_desc']); ?></p>
                          <div class="mu-latest-course-single-contbottom">
                            <a class="mu-course-details" href="topic_detail.php?v=<?php echo $fetch_topic_row['topic_name']; ?>">Details</a>
                          </div>
                        </div>
                      </div>
                    </div>
                      <?php
                    }  
                     ?>
</body>

</html>