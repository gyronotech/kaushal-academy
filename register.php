<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Register-Kaushal Skill Academy</title>

                <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Register-Kaushal Skill Academy</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>
<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-inner-pages">
    <div class="container d-flex align-items-center">
      <div class="contact-info mr-auto">
       
      </div>
      <div class="cta">
       
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.php#header" class="scrollto">KAUSHAL SKILL ACADEMY</a></h1>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="index.php#header">Home</a></li>
          <li><a href="index.php#about">About</a></li>
          <li><a href="index.php#services">Services</a></li>
          
          <li><a href="index.php#contact">Contact</a></li>
          <li><a href="index.php#team">Team</a></li>
      </nav>
      </nav>

    </div>
  </header>

  <main id="main">


  <br><br><br><br>
  <br><br><br>

  <div class="container  border shadow p-3 mb-5 bg-white rounded">
          <div class="container" style="padding:30px;">
          <form method="post">
                <div class="form-group">
                <label for="email">Email:</label>
                  <input name="username" type="email" class="form-control" placeholder="Your Email*" required>
                </div>


                <div class="form-group">              
                <label for="email">Password:</label>
                  <input name="password" type="password" class="form-control" placeholder="Your Password*" required>
                </div>


                <div class="form-group">               
                <label for="email">Confirm Password:</label>
                  <input name="password_match" type="password" class="form-control" placeholder="Your Password Again*" required>
                </div>


                <div class="row">
                  <div class="form-group col-md-6">
                <label for="email">First Name:</label>
                    <input name="first_name" type="text" class="form-control" placeholder="First Name *" required>
                  </div>


                  <div class="form-group col-md-6">                 
                <label for="email">Last Name:</label>
                    <input name="last_name" type="text" class="form-control" placeholder="Last Name *" required>
                  </div>
                  <br>


                  <div class="form-group col-md-12">                 
                <label for="email">Date:</label>
                    <input name="dob" type="date" class="form-control" placeholder="mm/dy/yy *" required>
                  </div>
                  <br>

                 </div>
               
                <div class="form-group">
                <label for="gender">Gender:</label>
                  <select name="gender" class="form-control" required>
                    <option selected disabled>Select Gender *</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                    <option value="Transgender">Transgender</option>
                  </select>
                </div>


                <div class="form-group">
                <label for="number">Number:</label>
                  <input name="phone" type="tel" pattern="[0-9]{10}" class="form-control" placeholder="Your Mobile Number *" required>
                </div>


                <div class="form-group">
                <label for="address">Address:</label>
                  <input name="address" type="text" class="form-control" placeholder="Your Address *" required>
                </div>


                <div class="form-group">
                <div class="con">

                  <button type="submit"  class="btn btn-primary" name="register">Sign Up</button>
                  <a class="btn btn-primary" href="login.php" >Log in</a>
                </div>
                </div>

                <div class="c">
                <span style= "font-size:10px;">By continuing, you agree to Anyar Academy's Conditions of Use and Privacy Policy.</span>
                </div>
              </form>
           </div>
         </div>
       </div>
     </div>
    </div>
  </section>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
 </body>
</html>
<?php
include_once "admin/conection.php";

if (isset($_POST['register']))
{
  $username = mysqli_real_escape_string($conn, $_POST['username']);
  echo$username;
  $password = mysqli_real_escape_string($conn, $_POST['password']);
  echo$password;
  $password_match = mysqli_real_escape_string($conn, $_POST['password_match']);
  echo$password_match;
  $first_name = mysqli_real_escape_string($conn, $_POST['first_name']);
  echo$first_name ;
  $last_name = mysqli_real_escape_string($conn, $_POST['last_name']);
  echo$last_name;
  $dob= mysqli_real_escape_string($conn, $_POST['dob']);
  echo$dob;
  $gender = mysqli_real_escape_string($conn, $_POST['gender']);
  echo$gender;

  $phone = mysqli_real_escape_string($conn, $_POST['phone']);
  echo$phone;
  $address = mysqli_real_escape_string($conn, $_POST['address']);
  echo$address;
  $entry_timestamp = date('Y-m-d H:i:s', time());
  echo$entry_timestamp;

  $fetch_username = $conn->prepare("SELECT username FROM user_website WHERE username = ?");
  $fetch_username->bind_param('s', $username);
  $fetch_username->execute();
  $fetch_username_res = $fetch_username->get_result();

  if($fetch_username_res->num_rows > 0)
  {
    ?><script>
    swal("Warning", "The email already exists. Log In to continue", "warning")
    .then((value) => {
      window.location.href="login.php";
    });
    </script>
    <?php
  }
  else
  {
    if ($password == $password_match)
      {
        $password_hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $insert_user = $conn->prepare("INSERT INTO user_website (username, password, first_name, last_name, dob, gender, phone, address, entry_timestamp)
        VALUES (?,?,?,?,?,?,?,?,?)");
        $insert_user->bind_param('sssssssss', $username, $password_hash, $first_name, $last_name, $dob, $gender, $phone, $address, $entry_timestamp);
        if($insert_user->execute())
        {
          ?><script>
          swal("You have been successfully registered", "Please log in to continue", "success")
          .then((value) => {
            window.location.href="login.php";
          });
          </script><?php
        }
        else
        {
          ?><script>
          swal("Error occurred while registering you", "Please try again in some time", "error")
          .then((value) => {
            window.history.back();
          });</script><?php
        }
      }
      else
      {
        ?><script>
        swal("The passwords do not match", "Make sure both passwords are identical to continue", "error")
        .then((value) => {
          window.history.back();
        });</script><?php
      }      
  }
}
$conn->close();
?>



