-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2021 at 09:08 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anyar`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` text DEFAULT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'casburn.noronha30@gmail.com', '$2y$10$NoQuYKdaULY6NM9TyxA43.1HOwIRYN8GhSypnNPl8hvC5opGwpR7S');

-- --------------------------------------------------------

--
-- Table structure for table `completion_record`
--

CREATE TABLE `completion_record` (
  `id` int(11) NOT NULL,
  `username_id` text DEFAULT NULL,
  `subtopic_name` text DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  `entry_timestamp` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `completion_record`
--

INSERT INTO `completion_record` (`id`, `username_id`, `subtopic_name`, `status`, `entry_timestamp`) VALUES
(1, 'amey@yahoo.in', '3', '1', '2021-03-03 08:56:46'),
(2, 'amey@yahoo.in', '1', '1', '2021-03-03 10:25:45'),
(3, 'amey@yahoo.in', '2', '1', '2021-03-03 10:25:56'),
(4, 'amey@yahoo.in', '3', '1', '2021-03-03 10:26:06'),
(5, 'amey@yahoo.in', '3', '1', '2021-03-03 10:26:07'),
(6, 'amey@yahoo.in', '1', '1', '2021-03-08 11:54:32'),
(7, 'amey@yahoo.in', '2', '1', '2021-03-08 11:54:44'),
(8, 'amey@yahoo.in', '3', '1', '2021-03-08 11:54:54'),
(9, 'amey@yahoo.in', '1', '1', '2021-03-08 12:03:25'),
(10, 'amey@yahoo.in', '2', '1', '2021-03-08 12:03:33'),
(11, 'amey@yahoo.in', '3', '1', '2021-03-08 12:03:41'),
(12, 'amey@yahoo.in', '3', '1', '2021-03-08 12:38:44'),
(13, '', '1', '1', '2021-06-12 13:10:43'),
(14, '', '2', '1', '2021-06-12 13:10:56'),
(15, 'amey@yahoo.in', '1', '1', '2021-06-12 14:04:57'),
(16, 'amey@yahoo.in', '44', '1', '2021-06-21 12:03:42'),
(17, 'amey@yahoo.in', '46', '1', '2021-06-21 12:03:56'),
(18, 'amey@yahoo.in', '46', '1', '2021-06-21 12:03:57'),
(19, 'amey@yahoo.in', '46', '1', '2021-06-21 12:03:57'),
(20, 'amey@yahoo.in', '46', '1', '2021-06-21 12:03:58'),
(21, 'amey@yahoo.in', '46', '1', '2021-06-21 12:03:58'),
(22, 'amey@yahoo.in', '46', '1', '2021-06-21 12:03:58'),
(23, 'amey@yahoo.in', '46', '1', '2021-06-21 12:03:59'),
(24, 'amey@yahoo.in', '46', '1', '2021-06-21 12:03:59'),
(25, 'amey@yahoo.in', '43', '1', '2021-06-23 11:46:46'),
(26, 'amey@yahoo.in', '1', '1', '2021-06-25 09:02:53'),
(27, '', '1', '1', '2021-06-28 12:50:26');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `course_id` int(11) NOT NULL,
  `course_name` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `course_name`) VALUES
(1, 'Engineering'),
(2, 'Football'),
(3, 'Cricket'),
(4, 'Dance'),
(5, 'scientist');

-- --------------------------------------------------------

--
-- Table structure for table `course_website`
--

CREATE TABLE `course_website` (
  `course_id` int(11) NOT NULL,
  `course_name` text DEFAULT NULL,
  `course_desc` text DEFAULT NULL,
  `course_img` text DEFAULT NULL,
  `entry_timestamp` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `course_website`
--

INSERT INTO `course_website` (`course_id`, `course_name`, `course_desc`, `course_img`, `entry_timestamp`) VALUES
(1, 'Engineering3456', 'detail of the course', 'content/course/20210617113910.jpg', '2021-02-01 12:48:15'),
(7, 'sci', 'detail of the course1', 'content/course/20210617111818.jpg', '2021-06-17 11:18:18'),
(9, 'Engineering11', 'SDG', 'content/course/20210623114539.jpg', '2021-06-23 11:45:39');

-- --------------------------------------------------------

--
-- Table structure for table `live_attendance`
--

CREATE TABLE `live_attendance` (
  `id` int(11) NOT NULL,
  `username` text DEFAULT NULL,
  `live_class_id` text DEFAULT NULL,
  `insert_timestamp` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `live_attendance`
--

INSERT INTO `live_attendance` (`id`, `username`, `live_class_id`, `insert_timestamp`) VALUES
(1, 'amey@yahoo.in', '1', '2021-02-01 13:29:57'),
(2, 'gourish2@yahoo.in', NULL, '2021-02-25 09:17:30'),
(3, 'gourish2@yahoo.in', NULL, '2021-02-25 09:17:37'),
(4, 'gourish2@yahoo.in', '1', '2021-02-25 09:17:52'),
(5, 'gourish2@yahoo.in', '2', '2021-02-25 10:42:02'),
(6, 'gourish2@yahoo.in', '3', '2021-02-25 11:08:45'),
(7, 'gourish2@yahoo.in', '4', '2021-02-26 06:04:05'),
(8, 'casburn.noronha30@gmail.com', '2', '2021-02-27 07:36:26'),
(9, 'amey@yahoo.in', '2', '2021-02-27 08:07:55'),
(10, 'amey@yahoo.in', '4', '2021-02-27 11:10:02'),
(11, 'matty@yahoo.in', '4', '2021-03-02 09:02:34');

-- --------------------------------------------------------

--
-- Table structure for table `live_class`
--

CREATE TABLE `live_class` (
  `id` int(11) NOT NULL,
  `vid_title` text DEFAULT NULL,
  `link` text DEFAULT NULL,
  `pass` text DEFAULT NULL,
  `live_status` enum('0','1') DEFAULT NULL,
  `insert_timestamp` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `live_class`
--

INSERT INTO `live_class` (`id`, `vid_title`, `link`, `pass`, `live_status`, `insert_timestamp`) VALUES
(1, 'testing', '1234', 'was', '1', '2021-02-03 15:09:10'),
(2, 'engi', '222', '12', '1', '2021-02-02 15:11:12'),
(3, 'dance', 'ee', 'ee', '1', '2021-02-01 15:27:07'),
(4, 'sci', 'https://meet.google.com/zux-qzcg-mss', '123', '0', '2021-03-01 15:17:09');

-- --------------------------------------------------------

--
-- Table structure for table `mcq`
--

CREATE TABLE `mcq` (
  `mcq_id` int(11) NOT NULL,
  `course_name` text DEFAULT NULL,
  `question` text DEFAULT NULL,
  `opt1` text DEFAULT NULL,
  `opt2` text DEFAULT NULL,
  `opt3` text DEFAULT NULL,
  `correct_opt` text DEFAULT NULL,
  `marks` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mcq`
--

INSERT INTO `mcq` (`mcq_id`, `course_name`, `question`, `opt1`, `opt2`, `opt3`, `correct_opt`, `marks`) VALUES
(1, 'Engineering', '\'OS\' computer abbrevaiation usually means', 'open software', 'optical sensor', 'operating system', 'operating system', '1'),
(2, 'Engineering', 'HTML is what type of language', 'network protocol', 'scripting language', 'markup language', 'markup language', '1');

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_temp`
--

CREATE TABLE `password_reset_temp` (
  `email` text NOT NULL,
  `key_id` text NOT NULL,
  `expDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `password_reset_temp`
--

INSERT INTO `password_reset_temp` (`email`, `key_id`, `expDate`) VALUES
('amey@yahoo.in', '12345', '2021-02-28 11:52:46'),
('gourish2@yahoo.in', '123', '2021-02-28 11:54:28'),
('amey@yahoo.in', '768e78024aa8fdb9b8fe87be86f647453b7e71c4fb', '2021-02-27 08:01:08'),
('amey@yahoo.in', '768e78024aa8fdb9b8fe87be86f647458bcf687e88', '2021-02-27 08:02:07'),
('amey@yahoo.in', '768e78024aa8fdb9b8fe87be86f647451ff023179a', '2021-02-27 08:02:10'),
('amey@yahoo.in', '768e78024aa8fdb9b8fe87be86f64745ed0fcd30b6', '2021-02-27 08:09:04'),
('amey@yahoo.in', '4c9a45f99398a50a0b7564fd6d1456c742546a15c1', '2021-02-27 08:25:38'),
('amey@yahoo.in', 'ee64c9b4a105d365879e963f0674d28e9cdc050032', '2021-02-27 08:27:47'),
('amey@yahoo.in', 'ee64c9b4a105d365879e963f0674d28eceea094cd3', '2021-02-27 08:27:51'),
('amey@yahoo.in', 'ee64c9b4a105d365879e963f0674d28ec72ec7f9bf', '2021-02-27 08:52:15'),
('amey@yahoo.in', 'ee64c9b4a105d365879e963f0674d28e824dcce5c9', '2021-02-27 08:52:20'),
('amey@yahoo.in', 'ee64c9b4a105d365879e963f0674d28e7069d9d6a3', '2021-02-27 08:55:17'),
('amey@yahoo.in', 'ee64c9b4a105d365879e963f0674d28e90aaeaa0aa', '2021-02-27 08:56:33'),
('amey@yahoo.in', 'ee64c9b4a105d365879e963f0674d28e066cac36a4', '2021-02-27 08:57:16'),
('amey@yahoo.in', 'ee64c9b4a105d365879e963f0674d28e0d286516dd', '2021-02-28 06:20:35'),
('casburn.noronha30@gmail.com', 'b82c5ab2ca7fd94c2d06e2a19afad0b7eda0ca5f49', '2021-02-28 07:37:21'),
('casburn.noronha30@gmail.com', 'b82c5ab2ca7fd94c2d06e2a19afad0b7ed1961cb4f', '2021-02-28 07:37:43'),
('casburn.noronha30@gmail.com', 'b82c5ab2ca7fd94c2d06e2a19afad0b7d99246f841', '2021-02-28 07:39:51'),
('casburn.noronha30@gmail.com', 'b82c5ab2ca7fd94c2d06e2a19afad0b77c1b254a54', '2021-02-28 07:39:56'),
('casburn.noronha30@gmail.com', 'b82c5ab2ca7fd94c2d06e2a19afad0b72ae519dc6a', '2021-02-28 07:40:18'),
('casburn.noronha30@gmail.com', 'b82c5ab2ca7fd94c2d06e2a19afad0b76366bc7c29', '2021-02-28 07:41:36'),
('casburn.noronha30@gmail.com', 'b82c5ab2ca7fd94c2d06e2a19afad0b779fb2ff8be', '2021-03-17 13:29:17');

-- --------------------------------------------------------

--
-- Table structure for table `subtopic_website`
--

CREATE TABLE `subtopic_website` (
  `id` int(11) NOT NULL,
  `subtopic_name` text DEFAULT NULL,
  `subtopic_desc` text DEFAULT NULL,
  `video_loc` text DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `question` text DEFAULT NULL,
  `entry_timestamp` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subtopic_website`
--

INSERT INTO `subtopic_website` (`id`, `subtopic_name`, `subtopic_desc`, `video_loc`, `course_id`, `question`, `entry_timestamp`) VALUES
(1, 'engineering1', 'into eng', 'content/vid/jetyy.mp4', 1, '1,2', '2021-02-02 12:41:04'),
(2, 'dance', 'scas', 'content/vid/jetyy.mp4', 1, '1,2', '2021-03-01 11:44:45'),
(3, 'crickt', 'scassca', 'content/vid/jetyy.mp4', 1, '1,2', '2021-03-01 11:44:45'),
(42, 'qwe', '<p>xx</p>', 'content/vid/20210311062158.mp4', NULL, NULL, '2021-03-11 06:21:58'),
(43, 'qwexcc', 'dada', 'content/vid/20210621085439youro.mp4', 7, '1,2', NULL),
(44, 'vrvr', 'dada', 'content/vid/20210621085833phntm.mp4', 8, '1,2', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` text DEFAULT NULL,
  `password` text DEFAULT NULL,
  `user_type` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `user_type`) VALUES
(1, 'admin', '12345', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user_website`
--

CREATE TABLE `user_website` (
  `id` int(11) NOT NULL,
  `username` text DEFAULT NULL,
  `password` text DEFAULT NULL,
  `first_name` text DEFAULT NULL,
  `last_name` text DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` text DEFAULT NULL,
  `phone` text DEFAULT NULL,
  `address` text DEFAULT NULL,
  `entry_timestamp` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_website`
--

INSERT INTO `user_website` (`id`, `username`, `password`, `first_name`, `last_name`, `dob`, `gender`, `phone`, `address`, `entry_timestamp`) VALUES
(1, 'amey@yahoo.in', '$2y$10$NoQuYKdaULY6NM9TyxA43.1HOwIRYN8GhSypnNPl8hvC5opGwpR7S', 'amey', 'volv', '2021-02-22', 'Male', '1234567890', 'dandora cuncolim', NULL),
(2, 'gourish2@yahoo.in', '$2y$10$pjzul4nSqakOiIYNagS05eOThSpzAX/WNBBEowU4EYIeZkhtWFFCy', 'gourish', 'bhat', '2021-02-09', 'Male', '1234567890', 'dandora cuncolim', '2021-02-24 09:14:48'),
(3, 'casburn.noronha30@gmail.com', '$2y$10$DoE.OmwOBYu5WnbU9LFlieG/iQV7u6IrkiMK7TWmPzn.s.BV2e426', 'casburn', 'noronha', '2021-02-01', 'Male', '8806227157', 'dandora cuncolim', '2021-02-27 07:35:54'),
(4, 'matty@yahoo.in', '$2y$10$lXAie1MJTpzCgKEzqJSCFeb7Qqo84bXr.dGgGYTyg.DtM0Il/Ci/K', 'matty', 'nor', '2021-03-01', 'Male', '8788440745', 'dandora cuncolim', '2021-03-02 07:50:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `completion_record`
--
ALTER TABLE `completion_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `course_website`
--
ALTER TABLE `course_website`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `live_attendance`
--
ALTER TABLE `live_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_class`
--
ALTER TABLE `live_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mcq`
--
ALTER TABLE `mcq`
  ADD PRIMARY KEY (`mcq_id`);

--
-- Indexes for table `subtopic_website`
--
ALTER TABLE `subtopic_website`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_website`
--
ALTER TABLE `user_website`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `completion_record`
--
ALTER TABLE `completion_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `course_website`
--
ALTER TABLE `course_website`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `live_attendance`
--
ALTER TABLE `live_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `live_class`
--
ALTER TABLE `live_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mcq`
--
ALTER TABLE `mcq`
  MODIFY `mcq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subtopic_website`
--
ALTER TABLE `subtopic_website`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_website`
--
ALTER TABLE `user_website`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
