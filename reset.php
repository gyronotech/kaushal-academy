<?php
include_once "admin/conection.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

if(isset($_POST["email"]) && (!empty($_POST["email"]))){
$email = $_POST["email"];
$email = filter_var($email, FILTER_SANITIZE_EMAIL);
$email = filter_var($email, FILTER_VALIDATE_EMAIL);
$error = "";
$username = $email;
if (!$email) {
   $error .="<p>Invalid email address please type a valid email address!</p>";
   }else{
   $sel_query = "SELECT * FROM `user_website` WHERE username='".$username."'";
   $results = mysqli_query($conn,$sel_query);
   $row = mysqli_num_rows($results);
   if ($row==""){
   $error .= "<p>No user is registered with this email address!</p>";
   }
  }
   if($error!=""){
   echo "<div class='error'>".$error."</div>
   <br /><a href='javascript:history.go(-1)'>Go Back</a>";
   }else{
   $expFormat = mktime(
   date("H"), date("i"), date("s"), date("m") ,date("d")+1, date("Y")
   );
   $expDate = date("Y-m-d H:i:s",$expFormat);
   $key = md5($email);
   $addKey = substr(md5(uniqid(rand(),1)),3,10);
   $key = $key . $addKey;
// Insert Temp Table
mysqli_query($conn,
"INSERT INTO `password_reset_temp` (`email`, `key`, `expDate`)
VALUES ('".$email."', '".$key."', '".$expDate."');");
 
$output='<p>Dear user,</p>';
$output.='<p>Please click on the following link to reset your password.</p>';
$output.='<p>-------------------------------------------------------------</p>';
$output.='<p><a href="reset-password.php?
key='.$key.'&email='.$email.'&action=reset" target="_blank">
reset-password.php
?key='.$key.'&email='.$email.'&action=reset</a></p>'; 
$output.='<p>-------------------------------------------------------------</p>';
$output.='<p>Please be sure to copy the entire link into your browser.
The link will expire after 1 day for security reason.</p>';
$output.='<p>If you did not request this forgotten password email, no action 
is needed, your password will not be reset. However, you may want to log into 
your account and change your security password as someone may have guessed it.</p>';   
$output.='<p>Thanks,</p>';
$output.='<p>AllPHPTricks Team</p>';
$body = $output; 
$subject = "Password Recovery - AllPHPTricks.com";
 
require("vendor/autoload.php");
$mail = new PHPMailer(true);
try
    {
$mail->setFrom('noreply@kaushalskillacademy.in', 'KAUSHALSKILLACADEMY');
// $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
$mail->addAddress($email);    

$mail->isHTML(true);                                  // Set email format to HTML
$mail->Subject = $subject;
$mail->Body    = $body;
$mail->AltBody = $body;
$mail->send();

?><script>alert('An email has been sent to you with instructions on how to reset your password.');window.location.href="login.php";</script><?php
}
catch (Exception $e)
{
  ?><script>alert('An unexpected error occurred. Please try again.');window.history.back();</script><?php
}


// $email_to = $email;
// $fromserver = "noreply@yourwebsite.com"; 

// $mail = new PHPMailer();
// $mail->IsSMTP();
// $mail->Host = "mail.yourwebsite.com"; // Enter your host here
// $mail->SMTPAuth = true;
// $mail->Username = "noreply@yourwebsite.com"; // Enter your email here
// $mail->Password = "password"; //Enter your password here
// $mail->Port = 25;
// $mail->IsHTML(true);
// $mail->From = "noreply@yourwebsite.com";
// $mail->FromName = "AllPHPTricks";
// $mail->Sender = $fromserver; // indicates ReturnPath header
// $mail->Subject = $subject;
// $mail->Body = $body;
//$mail->AddAddress($email_to);

if(!$mail->Send()){
echo "Mailer Error: " . $mail->ErrorInfo;
}else{
echo "<div class='error'>
<p>An email has been sent to you with instructions on how to reset your password.</p>
</div><br /><br /><br />";
 }
   }
}else{
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Reset Password-Kaushal Skill Academy</title>

                <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Reset-Kaushal Skill Academy</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>
<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-inner-pages">
    <div class="container d-flex align-items-center">
      <div class="contact-info mr-auto">
      
      </div>
      <div class="cta">
  
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.php#header" class="scrollto">Kaushal skill academy</a></h1>
      <nav class="nav-menu d-none d-lg-block">
      
      </nav>

    </div>
  </header>

  <main id="main">


    <br>
    <br><br><br><br><br>
 
    <div class="container  border shadow p-3 mb-5 bg-white rounded">
          <div class="container" style="padding:30px;">
           

            <form method="post">

        
<form method="post" action="" name="reset"><br /><br />
<label><strong><h2>Enter Your Email Address:</h2></strong></label><br /><br />
<input type="email" name="email" placeholder="Enter your email" />
<br /><br />
<input type="submit" value="Reset Password"/>
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<?php } ?>
                
                <span style="font-size:10px;">By continuing, you agree to Yashaswi Academy's Conditions of Use and Privacy Policy.</span>
              </form>
      </body>
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
      </html>















