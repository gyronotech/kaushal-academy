<?php

include_once 'admin/conection.php';

if(isset($_GET['v']))
{
  $course = $_GET['v'];
}

$cookie_name = "anyar";

$fetch_course = $conn->prepare("SELECT * FROM course_website WHERE course_id = ?");
$fetch_course->bind_param('i', $course);
$fetch_course->execute();
$fetch_course_res = $fetch_course->get_result();
$fetch_course_row = $fetch_course_res->fetch_assoc();

$fetch_subtopic = $conn->prepare("SELECT * FROM subtopic_website WHERE course_id = ?");
$fetch_subtopic->bind_param('i', $course);
$fetch_subtopic->execute();
$fetch_subtopic_res = $fetch_subtopic->get_result();

$get_name = $conn->prepare("SELECT first_name FROM user_website WHERE username = ?");
$get_name->bind_param('s', $_COOKIE[$cookie_name]);
$get_name->execute();
$get_name_rs = $get_name->get_result();
$get_name_row = $get_name_rs->fetch_assoc();

$get_status = $conn->prepare("SELECT * FROM completion_record WHERE username_id = ? AND subtopic_name = ? ");

?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Topic Detail-Kaushal Skill Academy</title>
  

  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

<!--plugn for the video -->
  <link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />


  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Anyar - v2.2.1
  * Template URL: https://bootstrapmade.com/anyar-free-multipurpose-one-page-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-inner-pages">
    <div class="container d-flex align-items-center">
      <div class="contact-info mr-auto">
      </div>
      <div class="cta">
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.php#header" class="scrollto"><h5>KAUSHAL SKILL ACADEMY</h5></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="index.php#header">Home</a></li>
          <li><a href="index.php#about">About</a></li>
          <li><a href="index.php#services">Services</a></li>
          <li><a href="index.php#contact">Contact</a></li>
          <li><a href="index.php#team">Team</a></li>
          <?php

if(isset($_COOKIE[$cookie_name]) != '')

{

  ?>
         
         <li><a href="live.php">Live Class</a></li>
         
         <li class="active"><a href="course.php">Courses</a></li>
       <li class="drop-down"><a href="">Welcome, <?php echo $get_name_row['first_name']; ?></a>
           <ul>
             <li><a href="logout.php?logout">Logout</a></li>
           </ul>
         </li>
       <?php
        
      }

      else

      {

        ?>

<li><a href="login.php?login">Courses</a></li>

<li><a href="login.php?login">Blog</a></li>

<li><a href="login.php?login">Live Classes</a></li>

<li><a href="login.php">Login</a></li>

<?php

}
?>
</ul>
</div>
</div>

      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="course.php">Course</a></li>
          <li><a>Details</a></li>
        </ol>
   

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container">

        <div class="row">

        <div class="col-lg-11 entries">

<article class="entry entry-single">
    <br>
      <div class="mu-latest-course-single">
        <figure class="mu-latest-course-img">
          <a><img src="<?php echo $fetch_course_row['course_img']; ?>" alt="img"></a>
        </figure>

<div class="mu-latest-course-single-content">

  <h2><a href="#"><?php echo $fetch_course_row['course_name']; ?></a></h2>

  <h4>Description</h4>

  <?php echo $fetch_course_row['course_desc']; ?>

  <!-- <h4>Course Outline</h4> -->

  <div class="table-responsive">

    <table class="table">

    <thead>

      <tr>

        <th> # </th>

        <th> Title </th>

        <th> Status </th>

      </tr>

    </thead>

    <tbody>

      <?php

      $i = 1;

      while ($fetch_subtopic_row = $fetch_subtopic_res->fetch_assoc())
      {
        echo '<tr><td>'.$i.'</td><td><a href="subtopic.php?i='.$fetch_subtopic_row['id'].'&c='.$course.'">'.$fetch_subtopic_row['subtopic_name'].'</a></td>';

        $get_status->bind_param('ss', $_COOKIE[$cookie_name], $fetch_subtopic_row['id']);

        $get_status->execute();

        $get_status_res = $get_status->get_result();

        $get_status_row = $get_status_res->fetch_assoc();

        if ($get_status_row['status'] == '0') {

          echo '<td> Ongoing </td></tr>';
        }

        elseif ($get_status_row['status'] == '1') 
        {
          echo '<td> Completed </td></tr>';
        }
        else
         {
          echo '<td></td></tr>';
        }
        $i++;
      }

      ?>

    </tbody>

  </table>

  </div>

</div>

</div>

</div>

</div>

</div>

<!-- end course content container -->

            
</body>

</html>


