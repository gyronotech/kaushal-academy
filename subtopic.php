<?php
include_once 'admin/conection.php';
$cookie_name = 'anyar';

if(isset($_GET['i']))
{
  $subtopic = $_GET['i'];
}
function clean($string) {
  $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
  return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

$get_name = $conn->prepare("SELECT first_name FROM user_website WHERE username = ?");
$get_name->bind_param('s', $_COOKIE[$cookie_name]);
$get_name->execute();
$get_name_rs = $get_name->get_result();
$get_name_row = $get_name_rs->fetch_assoc();

$get_status = $conn->prepare("SELECT * FROM completion_record WHERE username_id = ? AND subtopic_name = ?");
$get_status->bind_param('ss', $_COOKIE[$cookie_name], $subtopic);
$get_status->execute();
$get_status_res = $get_status->get_result();
$get_status_row = $get_status_res->fetch_assoc();

$fetch_subtopic = $conn->prepare("SELECT * FROM subtopic_website WHERE id = ?");
$fetch_subtopic->bind_param('s', $subtopic);
$fetch_subtopic->execute();
$fetch_subtopic_res = $fetch_subtopic->get_result();
$fetch_subtopic_row = $fetch_subtopic_res->fetch_assoc();

$course=$fetch_subtopic_row['course_id'];

//fetch_video
$temp = explode(".", $fetch_subtopic_row['video_loc']);
$extension = end($temp);

//fetch_questions
$question_array = explode(',', $fetch_subtopic_row['question']);
shuffle($question_array);

$fetch_next_course = $conn->prepare("SELECT * FROM subtopic_website WHERE course_id = ?");
$fetch_next_course->bind_param('i', $course);
$fetch_next_course->execute();
$fetch_next_course_res = $fetch_next_course->get_result();
while ($fetch_next_course_row = $fetch_next_course_res->fetch_assoc())
{
  $course_array[] = $fetch_next_course_row['id'];
}
print_r($course_array);
$index = array_search($subtopic, $course_array);
if(isset($course_array[$index+1]))
{
  $x = 'subtopic.php?i='.$course_array[$index+1].'&c='.$course;
}
else
{
  $x = 'topic_detail.php?v='.$course;
}

$question = $conn->prepare("SELECT * FROM mcq WHERE mcq_id = ?");

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Subtopic-Kaushal Skill Academy</title>
  <style>
  .invisible {
  visibility: hidden !important;
}
  </style>
  

  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

<!--plugn for the video -->
  <link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />


  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Anyar - v2.2.1
  * Template URL: https://bootstrapmade.com/anyar-free-multipurpose-one-page-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-inner-pages">
    <div class="container d-flex align-items-center">
      <div class="contact-info mr-auto">
      </div>
      <div class="cta">
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.php#header" class="scrollto"><h5>KAUSHAL SKILL ACADEMY</h5></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="index.php#header">Home</a></li>
          <li><a href="index.php#about">About</a></li>
          <li><a href="index.php#services">Services</a></li>
          <li><a href="index.php#contact">Contact</a></li>
          <li><a href="index.php#team">Team</a></li>
          <?php

if(isset($_COOKIE[$cookie_name]) != '')

{

  ?>
         
          <li><a href="live.php">Live Class</a></li>
         
          <li class="active"><a href="course.php">Courses</a></li>
          <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <?php echo $get_name_row['first_name']; ?> <span class="fa fa-angle-down"></span></a>
          <!--<ul class="dropdown-menu" role="menu">-->
          <li><a href="logout.php?logout">Logout</a></li>

        </ul>
        </li> <?php
        
      }

      else

      {

        ?>

<li><a href="login.php?login">Courses</a></li>

<li><a href="login.php?login">Blog</a></li>

<li><a href="login.php?login">Live Classes</a></li>

<li><a href="login.php">Login</a></li>

<?php

}
?>
</ul>
</div>
</div>

      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Home</a></li>
          <li><a>Subtopic</a></li>
        </ol>
      

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container">

        <div class="row">

          <div class="col-lg-11 entries">

            <article class="entry entry-single">
                <br>

                <div class="mu-latest-course-single">
                        <video id='myvideo' class='video-js vjs-default-skin vjs-big-play-centered' controls preload='auto' data-setup='{}' style="width:100%;" controlsList="nodownload">
                          <source src=<?php echo $fetch_subtopic_row['video_loc']; ?> type='video/<?php echo $extension; ?>'>
                          <p class='vjs-no-js'>
                            To view this video please enable JavaScript, and consider upgrading to a web browser that
                            <a href='https://videojs.com/html5-video-support/' target='_blank'>supports HTML5 video</a>
                          </p>
                        </video>

              <h2 class="entry-title">
                <a href="blog-single.html">Dolorum optio tempore voluptas dignissimos cumque fuga qui quibusdam quia</a>
              </h2>


              <div class="entry-content">
                <p>
                  Similique neque nam consequuntur ad non maxime aliquam quas. Quibusdam animi praesentium. Aliquam et laboriosam eius aut nostrum quidem aliquid dicta.
                  Et eveniet enim. Qui velit est ea dolorem doloremque deleniti aperiam unde soluta. Est cum et quod quos aut ut et sit sunt. Voluptate porro consequatur assumenda perferendis dolore.
                </p>

                <p>
                  Sit repellat hic cupiditate hic ut nemo. Quis nihil sunt non reiciendis. Sequi in accusamus harum vel aspernatur. Excepturi numquam nihil cumque odio. Et voluptate cupiditate.
                </p>    

                <p>
                  Sed quo laboriosam qui architecto. Occaecati repellendus omnis dicta inventore tempore provident voluptas mollitia aliquid. Id repellendus quia. Asperiores nihil magni dicta est suscipit perspiciatis. Voluptate ex rerum assumenda dolores nihil quaerat.
                  Dolor porro tempora et quibusdam voluptas. Beatae aut at ad qui tempore corrupti velit quisquam rerum. Omnis dolorum exercitationem harum qui qui blanditiis neque.
                  Iusto autem itaque. Repudiandae hic quae aspernatur ea neque qui. Architecto voluptatem magni. Vel magnam quod et tempora deleniti error rerum nihil tempora.
                </p> 

                <form class="invisible" id="quiz">
                            <?php
                            $i = 1;
                         // print_r($question_array);
                    
                            foreach ($question_array as $question_id)
                            {
                              $question->bind_param('i', $question_id);
                              $question->execute();
                              $question_res = $question->get_result();
                              $question_row = $question_res->fetch_assoc();

                              $option_array = array('opt1', 'opt2', 'opt3');
                              ?><div id="<?php echo $i; ?>" <?php if($i > 1){echo 'style="display:none;"';} ?>>
                                <div class="form-group">
                                  <div class="col-sm-2"><b>Question: <?php echo $i; ?></b></div>
                                  <div class="col-sm-10">
                              
                                    <p id="question"><?php echo $question_row['question']; ?></p>
                                  </div>
                                </div>
                                <p class="correct_answer" value="<?php echo stripslashes($question_row['correct_opt']); ?>"></p>
                                <?php
                                foreach ($option_array as $option)
                                {
                                  if (!empty($question_row[$option]))
                                  {
                                    ?>
                                    <button type="button" class="btn btn-outline-info btn-block text-justify" value="<?php echo clean($question_row[$option])."::".clean($question_row['correct_opt']); ?>" name="answer" onclick="check('<?php echo clean($question_row[$option])."::".clean($question_row['correct_opt']); ?>')"><span style="white-space: normal;"><?php echo stripslashes($question_row[$option]); ?></span></button>
                                    <br><br>
                                    <?php
                                  }
                                }
                                ?>
                                <div class="form-group">
                                  <?php if($i === count($question_array))
                                  {
                                    ?><a class="btn btn-primary" href="<?php echo $x; ?>">Next Topic</a><?php
                                  }
                                  else
                                  {
                                    echo '<a href="javascript:void(0)" class="btn btn-primary" onclick="hide('.$i.')">Next Question</a>';
                                  } ?>
                                </div>
                              </div><?php
                              $i++;
                            }
                            ?>

                          </form>

            </article><!-- End blog entry -->

  <!-- ======= Footer ======= -->
  
  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

<!-- jQuery library -->
<script src="assets/js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>
  <!-- Mixit slider -->
  <script type="text/javascript" src="assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="assets/js/jquery.fancybox.pack.js"></script>

 <!-- Custom js -->
 <script src="assets/js/custom.js"></script>
<!--plugin for the video to play-->
<script src="https://vjs.zencdn.net/7.10.2/video.min.js"></script>
  <script>
 if (document.getElementById("myvideo")) {
  videojs("myvideo").ready(function() {
    var myPlayer = this;
    var username = '<?php echo $_COOKIE[$cookie_name]; ?>';
    console.log(username);
    var subtopic = '<?php echo $subtopic; ?>';
    console.log(subtopic);
    //Set initial time to 0
    var currentTime = 0;
    myPlayer.on("seeking", function(event) {
      if (currentTime < myPlayer.currentTime()) {
        myPlayer.currentTime(currentTime);
      }
    });
    myPlayer.on("seeked", function(event) {
      if (currentTime < myPlayer.currentTime()) {
        myPlayer.currentTime(currentTime);
      }
    });
    setInterval(function() {
      if (!myPlayer.paused()) {
        currentTime = myPlayer.currentTime();
      }
    }, 1000);

    myPlayer.on('ended', function() {
      var dataString = "username="+username+"&subtopic="+subtopic+"&s=1";
      $.ajax({
        type: "POST",
        url: "post.php",
        data: dataString,
        success: function(result){
          console.log(result);
        }
      });
      $('#quiz').removeClass("invisible");
    });

    myPlayer.on('play', function(){
      var dataString = "username="+username+"&subtopic="+subtopic+"&s=0";
      $.ajax({
        type: "POST",
        url: "post.php",
        data: dataString
      });
    });

  });
}
  </script>
  <script>
  function hide(id){
    var inc = id+1;
    $("#"+ id +"").hide();
    $("#"+ inc +"").show();
  }

  function check(str) {
    var arr = str.split("::");
    var fired_button = arr[0];
    var correct_opt = arr[1];
    console.log(fired_button);
    console.log(correct_opt);
    console.log(str);
    if (correct_opt === fired_button) {
      $('button[value="'+str+'"]').removeClass( "btn-outline-info" ).addClass( "btn-success" );
    }
    else {
      $('button[value="'+str+'"]').removeClass( "btn-outline-info" ).addClass( "btn-danger" );
      $('button[value="'+arr[1]+'::'+arr[1]+'"]').removeClass( "btn-outline-info" ).addClass( "btn-success" );
    }
  }
  </script>

</body>

</html>