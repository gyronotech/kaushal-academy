<?php
include_once 'conection.php';
// (A) FUNCTION TO FORMULATE SERVER RESPONSE
function verbose($ok=1,$info=""){
  // THROW A 400 ERROR ON FAILURE
  if ($ok==0) { http_response_code(400); }
  die(json_encode(["ok"=>$ok, "info"=>$info]));
}
// (B) INVALID UPLOAD
if (empty($_FILES) || $_FILES['file']['error']) {
  verbose(0, "Failed to move uploaded file.");
}
$filePath = "../content/vid";
$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];
$fileName = date('YmdHis',time()).$fileName;
// $filename=$_REQUEST["item1"];
$filePath = $filePath . DIRECTORY_SEPARATOR . $fileName;

$vid ="content/vid/".$fileName;

// (D) DEAL WITH CHUNKS
$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
if ($out) {
  $in = @fopen($_FILES['file']['tmp_name'], "rb");
  if ($in) {
    while ($buff = fread($in, 4096)) { fwrite($out, $buff); }
  } else {
    verbose(0, "Failed to open input stream");
  }
  @fclose($in);
  @fclose($out);
  @unlink($_FILES['file']['tmp_name']);

} else {
  verbose(0, "Failed to open output stream");
}
// (E) CHECK IF FILE HAS BEEN UPLOADED
if (!$chunks || $chunk == $chunks - 1) {
  rename("{$filePath}.part", $filePath);

  $sub = $_REQUEST['subtopic_name'];
  // echo $sub;
  $desc = $_REQUEST['subtopic_desc'];
  // echo $desc;
  $course = $_REQUEST['course_name'];
  // echo $course;
 $insert_info = $conn->prepare("INSERT INTO subtopic_website (subtopic_name,subtopic_desc,course_id,video_loc) VALUES (?,?,?,?)");
 $insert_info->bind_param('ssis', $sub,$desc,$course,$vid);
 $insert_info->execute();
}
verbose(1, "Upload OK");
?>

