

<?php
include_once 'conection.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

$user_name_cookie = "radiostore";

if(isset($_COOKIE[$user_name_cookie]) != '')
{
  header('Location:home.php');
}

if(isset($_POST["email"]) && (!empty($_POST["email"])))
{
  $email = $_POST["email"];
  $error = '';
  if (!$email)
  {
    $error .="Invalid email address please type a valid email address!";
  }
  else
  {
    $check_user = $conn->prepare('SELECT * FROM admin WHERE username = ?');
    $check_user->bind_param('s', $email);
    $check_user->execute();
    $check_user_res = $check_user->get_result();

    if ($check_user_res->num_rows == '0')
    {
      $error .= "No admin is registered with this email address";
    }
  }

  if($error!="")
  {
  ?><script>alert('<?php echo $error; ?>');window.history.back();</script><?php
  }
  else
  {
    $expFormat = mktime(date("H"), date("i"), date("s"), date("m") ,date("d")+1, date("Y"));
    $expDate = date("Y-m-d H:i:s",$expFormat);
    // $key = md5(2418*2+$email);
    $key = md5($email);
    $addKey = substr(md5(uniqid(rand(),1)),3,10);
    $key = $key . $addKey;

    $insert_reset = $conn->prepare('INSERT INTO password_reset_temp(email, key_id, expDate) VALUES (?,?,?)');
    $insert_reset->bind_param('sss', $email, $key, $expDate);
    $insert_reset->execute();

    $output='<p>Dear user,</p>';
    $output.='<p>Please click on the following link to reset your password.</p>';
    $output.='<p>-------------------------------------------------------------</p>';
    $output.='<p><a href="https://www.findmymeds.in/pharmacy/reset_password.php?key='.$key.'&email='.$email.'&action=reset" target="_blank">
    https://www.findmymeds.in/pharmacy/reset_password.php?key='.$key.'&email='.$email.'&action=reset</a></p>';
    $output.='<p>-------------------------------------------------------------</p>';
    $output.='<p>Please be sure to copy the entire link into your browser.
    The link will expire after 1 day for security reason.</p>';
    $output.='<p>If you did not request this forgotten password email, no action
    is needed, your password will not be reset. However, you may want to log into
    your account and change your security password as someone may have guessed it.</p>';
    $output.='<p>Thanks,</p>';
    $output.='<p>kaushalskillacademy Team</p>';
    $body = $output;
    $subject = "Pharmacy Password Recovery - kaushalskillacademy.in";

    // Load Composer's autoloader
    require '../vendor/autoload.php';

    // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try
    {
      //Recipients
      $mail->setFrom('noreply@kaushalskillacademy.in', 'kaushalskillacademy');
      // $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
      $mail->addAddress($email);               // Name is optional


      // Content
      $mail->isHTML(true);                                  // Set email format to HTML
      $mail->Subject = $subject;
      $mail->Body    = $body;
      $mail->AltBody = $body;

      $mail->send();
      ?><script>alert('An email has been sent to you with instructions on how to reset your password.');window.location.href="indx.php";</script><?php
    }
    catch (Exception $e)
    {
      ?><script>alert('An unexpected error occurred. Please try again.');window.history.back();</script><?php
    }
  }
}

if(isset($_POST['username']))
{
  $username = $_POST['username'];
  $password = $_POST['password'];
//   echo $user;
// echo $password;
  $res = $conn->prepare("SELECT * FROM admin WHERE username = ?");
  $res->bind_param('s', $username);
  $res->execute();
  $result = $res->get_result();
  $row = $result->fetch_assoc();
  if(password_verify($password, $row['password']))
  {
		$user_name_cookie_value = $row['username'];
		setcookie($user_name_cookie, $user_name_cookie_value, time() + (10 * 365 * 24 * 60 * 60), '/'); // cookie expires in 10 years
		header("Location: home.php");
        
  }
  else
  {
    ?><script>alert('Please enter correct credentials and try again.');window.location.href="indx.php";</script><?php
  }
}
$conn->close();
?>
<!DOCTYPE html>
<html lang="en" >
    <!--begin::Head-->
    <head>
                <meta charset="utf-8"/>
					<title>Radio Store</title>
                <meta name="description" content="Login page example"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

        <!--begin::Fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>        <!--end::Fonts-->


                    <!--begin::Page Custom Styles(used by this page)-->
                             <link href="assets/css/pages/login/classic/login-1.css" rel="stylesheet" type="text/css"/>
                        <!--end::Page Custom Styles-->

        <!--begin::Global Theme Styles(used by all pages)-->
                    <link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/>
                    <link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css"/>
                    <link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css"/>
                <!--end::Global Theme Styles-->

        <!--begin::Layout Themes(used by all pages)-->
                <!--end::Layout Themes-->
               

            </head>
    <!--end::Head-->

    <!--begin::Body-->
    <body  id="kt_body"  class="header-fixed header-mobile-fixed subheader-enabled page-loading"  >

    	<!--begin::Main-->
	<div class="d-flex flex-column flex-root">
		<!--begin::Login-->
<div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
    <!--begin::Aside-->
    <div class="login-aside d-flex flex-row-auto bgi-size-cover bgi-no-repeat p-10 p-lg-10" style="background-image: url(assets/media/bg/bg-4.jpg);">
        <!--begin: Aside Container-->
        <div class="d-flex flex-row-fluid flex-column justify-content-between">
            <!--begin: Aside header-->
            <a href="#" class="flex-column-auto mt-5 pb-lg-0 pb-10">
			
			</a>
            <!--end: Aside header-->

            <!--begin: Aside content-->
            <div class="flex-column-fluid d-flex flex-column justify-content-center">
                <h3 class="font-size-h1 mb-5 text-white">Welcome to Kaushal Skill Academy Admin!</h3>
                <p class="font-weight-lighter text-white opacity-80">
                   
                </p>
            </div>
            <!--end: Aside content-->

            <!--begin: Aside footer for desktop-->
            <div class="d-none flex-column-auto d-lg-flex justify-content-between mt-10">
                <div class="opacity-70 font-weight-bold	text-white">
                    &copy; <?php echo date('Y'); ?> FindmyMeds
                </div>
                <div class="d-flex">
                    <a href="#" class="text-white">Privacy</a>
                    <a href="#" class="text-white ml-10">Legal</a>
                    <a href="#" class="text-white ml-10">Contact</a>
                </div>
            </div>
            <!--end: Aside footer for desktop-->
        </div>
        <!--end: Aside Container-->
    </div>
    <!--begin::Aside-->

    <!--begin::Content-->
    <div class="d-flex flex-column flex-row-fluid position-relative p-7 overflow-hidden">
        <!--begin::Content header-->
        <div class="position-absolute top-0 right-0 text-right mt-5 mb-15 mb-lg-0 flex-column-auto justify-content-center py-5 px-10">
            <!-- <span class="font-weight-bold text-dark-50">Dont have an account yet?</span> -->
            <a href="emp_login.php" class="font-weight-bold ml-2"></a>
        </div>
        <!--end::Content header-->

        <!--begin::Content body-->
        <div class="d-flex flex-column-fluid flex-center mt-30 mt-lg-0">
            <!--begin::Signin-->
            <div class="login-form login-signin">
                <div class="text-center mb-10 mb-lg-20">
                    <h3 class="font-size-h1">Sign In</h3>
                    <p class="text-muted font-weight-bold">Enter your username and password</p>
                </div>

                <!--begin::Form-->
                <form class="form" method="post" novalidate="novalidate" id="kt_login_signin_form">
                    <div class="form-group">
                        <input class="form-control form-control-solid h-auto py-5 px-6" type="text" placeholder="Username" name="username" autocomplete="off" required/>
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-solid h-auto py-5 px-6" type="password" placeholder="Password" name="password" autocomplete="off" required/>
                    </div>
                    <!--begin::Action-->
                    <div class="form-group d-flex flex-wrap justify-content-between align-items-center">
                        <a href="javascript:;" class="text-dark-50 text-hover-primary my-3 mr-2" id="kt_login_forgot">
    						Forgot Password ?
    					</a>
                        <button type="submit" id="kt_login_signin_submit" name="login" class="btn btn-primary font-weight-bold px-9 py-4 my-3">Sign In</button>
                    </div>
                    <!--end::Action-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Signin-->

            <!--begin::Forgot-->
            <div class="login-form login-forgot">
                <div class="text-center mb-10 mb-lg-20">
                    <h3 class="font-size-h1">Forgotten Password ?</h3>
                    <p class="text-muted font-weight-bold">Enter your email to reset your password</p>
                </div>

                <!--begin::Form-->
                <form class="form" method="post" novalidate="novalidate" id="kt_login_forgot_form">
                    <div class="form-group">
                        <input class="form-control form-control-solid h-auto py-5 px-6" type="email" placeholder="Email" name="email" autocomplete="off" required/>
                    </div>
                    <div class="form-group d-flex flex-wrap flex-center">
                        <button type="button" id="kt_login_forgot_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Submit</button>
                        <button type="button" id="kt_login_forgot_cancel" class="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4">Cancel</button>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Forgot-->
        </div>
        <!--end::Content body-->

		<!--begin::Content footer for mobile-->
		<div class="d-flex d-lg-none flex-column-auto flex-column flex-sm-row justify-content-between align-items-center mt-5 p-5">
			<div class="text-dark-50 font-weight-bold order-2 order-sm-1 my-2">
				&copy; <?php echo date('Y'); ?> FindmyMeds
			</div>
			<div class="d-flex order-1 order-sm-2 my-2">
				<a href="#" class="text-dark-75 text-hover-primary">Privacy</a>
				<a href="#" class="text-dark-75 text-hover-primary ml-4">Legal</a>
				<a href="#" class="text-dark-75 text-hover-primary ml-4">Contact</a>
			</div>
		</div>
		<!--end::Content footer for mobile-->
    </div>
    <!--end::Content-->
</div>
<!--end::Login-->
	</div>
<!--end::Main-->

        <!--begin::Global Config(global config for global JS scripts)-->
        <script>
            var KTAppSettings = {
    "breakpoints": {
        "sm": 576,
        "md": 768,
        "lg": 992,
        "xl": 1200,
        "xxl": 1200
    },
    "colors": {
        "theme": {
            "base": {
                "white": "#ffffff",
                "primary": "#6993FF",
                "secondary": "#E5EAEE",
                "success": "#1BC5BD",
                "info": "#8950FC",
                "warning": "#FFA800",
                "danger": "#F64E60",
                "light": "#F3F6F9",
                "dark": "#212121"
            },
            "light": {
                "white": "#ffffff",
                "primary": "#E1E9FF",
                "secondary": "#ECF0F3",
                "success": "#C9F7F5",
                "info": "#EEE5FF",
                "warning": "#FFF4DE",
                "danger": "#FFE2E5",
                "light": "#F3F6F9",
                "dark": "#D6D6E0"
            },
            "inverse": {
                "white": "#ffffff",
                "primary": "#ffffff",
                "secondary": "#212121",
                "success": "#ffffff",
                "info": "#ffffff",
                "warning": "#ffffff",
                "danger": "#ffffff",
                "light": "#464E5F",
                "dark": "#ffffff"
            }
        },
        "gray": {
            "gray-100": "#F3F6F9",
            "gray-200": "#ECF0F3",
            "gray-300": "#E5EAEE",
            "gray-400": "#D6D6E0",
            "gray-500": "#B5B5C3",
            "gray-600": "#80808F",
            "gray-700": "#464E5F",
            "gray-800": "#1B283F",
            "gray-900": "#212121"
        }
    },
    "font-family": "Poppins"
};
        </script>
        <!--end::Global Config-->

    	<!--begin::Global Theme Bundle(used by all pages)-->
    	    	   <script src="assets/plugins/global/plugins.bundle.js"></script>
		    	   <script src="assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
		    	   <script src="assets/js/scripts.bundle.js"></script>
				<!--end::Global Theme Bundle-->


                    <!--begin::Page Scripts(used by this page)-->
                            <script src="assets/js/pages/custom/login/login-general.js"></script>
                        <!--end::Page Scripts-->
            </body>
    <!--end::Body-->
</html>
