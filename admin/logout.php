<?php
require_once 'conection.php';
session_start();


$username = $_SESSION['username'];

if(!isset($_SESSION['username']))
{
 header("Location: index.php");
}

if(isset($_GET['logout']))
{
  session_destroy();
  unset($_SESSION['username']);
  unset($_SESSION['user_type']);
  header("Location: index.php");
}
?>