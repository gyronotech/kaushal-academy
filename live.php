<?php
include_once "admin/conection.php";
$cookie_name = 'anyar';
if(!isset($_COOKIE[$cookie_name]))
{
  header("Location:login.php?login");
}

if (isset($_GET['pageno']))
{
  $pageno = $_GET['pageno'];
}
else
{
  $pageno = 1;
}
$no_of_records_per_page = 9;
$offset = ($pageno-1) * $no_of_records_per_page;

$total_pages_sql = 'SELECT COUNT(*) FROM live_class WHERE live_status = "0"';
$result = mysqli_query($conn,$total_pages_sql);
$total_rows = mysqli_fetch_array($result)[0];
echo $total_rows;
$total_pages = ceil($total_rows / $no_of_records_per_page);

$get_name = $conn->prepare("SELECT first_name FROM user_website WHERE username = ?");
$get_name->bind_param('s', $_COOKIE[$cookie_name]);
$get_name->execute();
$get_name_rs = $get_name->get_result();
$get_name_row = $get_name_rs->fetch_assoc();

$fetch_live = $conn->prepare('SELECT * FROM live_class WHERE live_status = "0"');//0 is live, 1 is offline
$fetch_live->execute();
$fetch_live_res = $fetch_live->get_result();
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Live Class-Kaushal Skill Academy</title>
  <style>
  .dataTables_paginate {
   float: none !important;
   text-align: center !important;
}</style>
  

  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

<!--plugn for the video -->
  <link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />


  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Anyar - v2.2.1
  * Template URL: https://bootstrapmade.com/anyar-free-multipurpose-one-page-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-inner-pages">
    <div class="container d-flex align-items-center">
      <div class="contact-info mr-auto">
      </div>
      <div class="cta">
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.html#header" class="scrollto"><h5>KAUSHAL SKILL ACADEMY</h5></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="index.php#header">Home</a></li>
          <li><a href="index.php#about">About</a></li>
          <li><a href="index.php#services">Services</a></li>
          <li><a href="index.php#contact">Contact</a></li>
          <li><a href="index.php#team">Team</a></li>
          <?php

if(isset($_COOKIE[$cookie_name]) != '')

{

  ?>
         
         <li class="active"><a href="live.php">Live Class</a></li>
         
         <li class><a href="course.php">Courses</a></li>
       <li class="drop-down"><a href="">Welcome, <?php echo $get_name_row['first_name']; ?></a>
           <ul>
             <li><a href="logout.php?logout">Logout</a></li>
           </ul>
         </li>
       <?php
      }

      else

      {

        ?>

<li><a href="login.php?login">Courses</a></li>

<li><a href="login.php?login">Blog</a></li>

<li><a href="login.php?login">Live Classes</a></li>

<li><a href="login.php">Login</a></li>

<?php

}
?>
</ul>
</div>
</div>

      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Course</a></li>
          <li><a>Details</a></li>
        </ol>
        <h2>Live Class</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
   <section id="blog" class="blog">
                <!-- start course content container -->
                <!-- <div class="mu-course-container"> -->
                  <!-- <div class="clearfix"> -->
                    
                  <?php
                  if($fetch_live_res->num_rows > 0)
                  {
                    ?><div class="container">
                      <div class="row">
                        
                        
                        <?php
                    while ($fetch_live_row = $fetch_live_res->fetch_assoc())
                    {
                      ?>
                      <div class="col-md-6 col-sm-6">
                      <div class="mu-latest-course-single">
                        <!-- <figure class="mu-latest-course-img">
                          <a href="class.php?v=<?php echo $fetch_live_row['id']; ?>"><img src="assets/img/live.jpg" alt="img"></a>
                        </figure> -->
                        <div class="mu-latest-course-single-content">
                          <h4><a href="class.php?v=<?php echo $fetch_live_row['id']; ?>"><?php echo $fetch_live_row['vid_title']; ?></a></h4>

                          <div class="mu-latest-course-single-contbottom">
                            <a class="mu-course-details" href="class.php?v=<?php echo $fetch_live_row['id']; ?>">class going on </a>
                          </div>    
                        </div>                      
                      </div>
                    </div>
                    <?php
                    }
                    ?></div>
                  </div><?php
                  }
                  ?>
                  <!-- </div> -->
                </div>
                <!-- end course content container -->
                <!-- start course pagination -->
             
                <ul class="pagination justify-content-center" style="margin:20px 0">
                  <li class="page-item"><a href="?pageno=1" class="page-link">First</a></li>
                  <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?> page-item">
                    <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>" class="page-link">Prev</a>
                  </li>
                  <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?> page-item">
                    <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>" class="page-link">Next</a>
                  </li>
                  <li class="page-item"><a href="?pageno=<?php echo $total_pages; ?>" class="page-link">Last</a></li>
                </ul>
                <!-- end course pagination -->
                
              </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
</body>

</html>