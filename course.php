<?php

include_once "admin/conection.php";

$cookie_name = 'anyar';

$fetch_course = $conn->prepare("SELECT * FROM course_website ORDER BY entry_timestamp ");
$fetch_course->execute();
$fetch_course_res = $fetch_course->get_result();

$get_name = $conn->prepare("SELECT first_name FROM user_website WHERE username = ?");
$get_name->bind_param('s', $_COOKIE[$cookie_name]);
$get_name->execute();
$get_name_rs = $get_name->get_result();
$get_name_row = $get_name_rs->fetch_assoc();

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Courses-Kaushal Skill Academy</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Anyar - v2.2.1
  * Template URL: https://bootstrapmade.com/anyar-free-multipurpose-one-page-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-inner-pages">
    <div class="container d-flex align-items-center">
      <div class="contact-info mr-auto">
      
      </div>
      <div class="cta">
     
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h5 class="logo mr-auto"><a href="index.php#header" class="scrollto"><h5>KAUSHAL SKILL ACADEMY</h5></a></h5>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="index.php#header">Home</a></li>
          <li><a href="index.php#about">About</a></li>
          <li><a href="index.php#services">Services</a></li>
          
          <li><a href="index.php#contact">Contact</a></li>
          <li><a href="index.php#team">Team</a></li>
          <?php

if(isset($_COOKIE[$cookie_name]) != '')

{

  ?>
         
         <li><a href="live.php">Live Class</a></li>
         
         <li class="active"><a href="course.php">Courses</a></li>
       <li class="drop-down"><a href="">Welcome, <?php echo $get_name_row['first_name']; ?></a>
           <ul>
             <li><a href="logout.php?logout">Logout</a></li>
           </ul>
         </li>
       <?php
      }

      else

      {

        ?>

<li><a href="login.php?login">Courses</a></li>

<li><a href="login.php?login">Blog</a></li>

<li><a href="login.php?login">Live Classes</a></li>

<li><a href="login.php">Login</a></li>

<?php

}
?>
</ul>
</div>
</div>

      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Home</a></li>
          <li>Course</li>
        </ol>
        <h2>Courses</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    
    <div class="container border shadow p-3 mb-5 bg-white rounded">
    <div class="card-body">
    
     <!-- <div class="form-group row"> -->
    <section id="blog" class="blog">
      <!-- <div class="container"> -->
<!-- 
        <div class="row"> -->

        <div class="col-lg-4  col-md-6 ">
            <!-- <article class="entry"> -->

            <?php

while ($fetch_course_row = $fetch_course_res->fetch_assoc())

{

  ?>
    <div class="entry-img">

      <a href="topic_detail.php?v=<?php echo stripslashes($fetch_course_row['course_id']); ?>"><img src="<?php echo $fetch_course_row['course_img']; ?>" class="img-responsive" alt="img"></a>

</div>
      <h4><a href="topic_detail.php?v=<?php echo stripslashes($fetch_course_row['course_id']); ?>"><?php echo stripslashes($fetch_course_row['course_name']); ?></a></h4>

      <p><?php echo stripslashes($fetch_course_row['course_desc']); ?></p>

      <div class="mu-latest-course-single-contbottom">

        <a class="mu-course-details" href="topic_detail.php?v=<?php echo stripslashes($fetch_course_row['course_id']); ?>">Details</a>

      </div>

    <!-- </div> -->

  <!-- </div> -->

<!-- </article> -->
</section>

<?php

}

?>

</div>
</div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>