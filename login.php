<?php
  include_once "admin/conection.php";
  if(isset($_POST['login']))
  {
    $user = mysqli_real_escape_string($conn, $_POST['username']);
    echo$user;
    $password = mysqli_real_escape_string($conn, $_POST['password']);
    echo$password;
    $res = $conn->prepare("SELECT * FROM user_website WHERE username = ?");
    $res->bind_param('s', $user);
    $res->execute();
    $result = $res->get_result();
    $row = $result->fetch_assoc();
    if(password_verify($password, $row['password']))
    {
      $cookie_value = $row['username'];
      echo $cookie_value;
      $cookie_name = 'anyar';
      setcookie($cookie_name, $cookie_value, time() + (86400 * 30), '/'); // 86400 = 1 day * 30 so cookie remains valid till 30 days.
      header('Location: course.php');
    }
    else
    {
      ?><script>
        alert('Please enter correct details and try again'); window.histrory.back();</script><?php
    }
    $conn->close();
  }
  ?>
 <!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Login-Kaushal Skill Academy</title>

                <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Login-Kaushal Skill Academy</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>
<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-inner-pages">
    <div class="container d-flex align-items-center">
      <div class="contact-info mr-auto">
       
      </div>
      <div class="cta">
   
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.php#header" class="scrollto">KAUSHAL SKILL ACADEMY</a></h1>
 
      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="index.php#header">Home</a></li>
          <li><a href="index.php#about">About</a></li>
          <li><a href="index.php#services">Services</a></li>
          
          <li><a href="index.php#contact">Contact</a></li>
          <li><a href="index.php#team">Team</a></li>
      </nav>

    </div>
  </header>

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Home</a></li>
          <li>Login</li>
        </ol>
        <h2>Login</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <br>

 
    <div class="container  border shadow p-3 mb-5 bg-white rounded">
          <div class="container" style="padding:30px;">
           

            <form method="post">

                <div class="form-group">
                <label for="email">Email:</label>
                  <input type="text" class="form-control" name="username" placeholder="Your Email *" value="" />
                </div>

                <div class="form-group">
                <label for="pwd">Password:</label>
                  <input type="password" class="form-control" name="password" placeholder="Your Password *" value="" />
                  
              
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-primary" name="login">Log In</button>
                  <a href="register.php" class="btn btn-primary">Create Account</a>
                </div>
                <a href="reset.php">forgot password?</a>
                <br>
                
                
                <span style="font-size:10px;">By continuing, you agree to Yashaswi Academy's Conditions of Use and Privacy Policy.</span>
              </form>
      </body>
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
      </html>



     
 