<?php
include_once 'admin/conection.php';

$cookie_name = 'anyar';
if(!isset($_COOKIE[$cookie_name]))
{
  header("Location:login.php?login");
}

$get_name = $conn->prepare("SELECT first_name FROM user_website WHERE username = ?");
$get_name->bind_param('s', $_COOKIE[$cookie_name]);
$get_name->execute();
$get_name_rs = $get_name->get_result();
$get_name_row = $get_name_rs->fetch_assoc();

if (isset($_GET['v']))
{
    $id = $_GET['v'];
}

//check status of the live stream and redirect to homepage if not live
$fetch_stream = $conn->prepare('SELECT * FROM live_class WHERE id = ?');
$fetch_stream->bind_param('i', $id);
$fetch_stream->execute();
$fetch_stream_res = $fetch_stream->get_result();
$fetch_stream_row = $fetch_stream_res->fetch_assoc();

$insert_timestamp = date('Y-m-d H:i:s', time());

$check_user = $conn->prepare('SELECT * FROM live_attendance WHERE username = ? AND live_class_id = ?');
$check_user->bind_param('si', $_COOKIE[$cookie_name], $id);
$check_user->execute();
$check_user_res = $check_user->get_result();

if($check_user_res->num_rows == 0)
{
  $mark_attendance = $conn->prepare('INSERT INTO live_attendance(username, live_class_id, insert_timestamp) VALUES (?,?,?)');
  $mark_attendance->bind_param('sss', $_COOKIE[$cookie_name], $id, $insert_timestamp);
  $mark_attendance->execute();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Blog Single - Serenity Bootstrap Template</title>
  

  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

<!--plugn for the video -->
  <link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />


  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Anyar - v2.2.1
  * Template URL: https://bootstrapmade.com/anyar-free-multipurpose-one-page-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-inner-pages">
    <div class="container d-flex align-items-center">
      <div class="contact-info mr-auto">
      </div>
      <div class="cta">
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.html#header" class="scrollto">Anyar</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html#header" class="logo mr-auto scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="index.html#header">Home</a></li>
          <li><a href="index.html#about">About</a></li>
          <li><a href="index.html#services">Services</a></li>
          <li><a href="index.html#portfolio">Portfolio</a></li>
          <li><a href="index.html#team">Team</a></li>
          <li><a href="index.html#pricing">Pricing</a></li>
          <li class="active"><a href="blog.html">Courses</a></li>
          <li class="drop-down"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="drop-down"><a href="#">Deep Drop Down</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
            </ul>
          </li>
          <li><a href="index.html#contact">Contact</a></li>

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.html">Course</a></li>
          <li><a href="blog.html">Details</a></li>
        </ol>
      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-12">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div>
                      <div class="mu-latest-course-single">
                        <div class="container" style="overflow: hidden; padding-top: 50%; position: relative;">
                          <!-- <iframe src="https://zoom.us/wc/<?php //echo $fetch_stream_row['link']; ?>/join?prefer=1&un=TWluZGF1Z2Fz" sandbox="allow-forms allow-scripts" allow="microphone; camera; fullscreen"></iframe> -->
                          <!-- <iframe allow="microphone; camera" style="border: 0; height: 100%; left: 0; position: absolute; top: 0; width: 100%;" src="https://success.zoom.us/wc/join/<?php //echo $fetch_stream_row['link']; ?>" frameborder="0"></iframe> -->
                        </div>
                      </div>
                      <!-- here will come the quiz logic with next button for each question and next topic button for the last question -->
                    </div>
                  </div>
                </div>
                <!-- end course content container -->
              </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>

  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>
  <!-- Mixit slider -->
  <script type="text/javascript" src="assets/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="assets/js/jquery.fancybox.pack.js"></script>

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script>
  <input type="text" id="pass" value="<?php echo $fetch_stream_row['pass']; ?>">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <style>
  .swal2-popup
  {
    font-size: 1.75rem;
  }
  </style>
  <script>
  let timerInterval
Swal.fire({
  title: 'Redirecting',
  html: 'You will be redirected to the session now.',
  timer: 1500,
  timerProgressBar: true,
  onClose: () => {
    clearInterval(timerInterval)
  }
}).then((result) => {
  /* Read more about handling dismissals below */
  if (result.dismiss === Swal.DismissReason.timer) {
    window.location.href= '<?php echo $fetch_stream_row['link']; ?>';
  }
  else {
    window.location.href= '<?php echo $fetch_stream_row['link']; ?>';
  }
})
  </script>
</body>

</html>
